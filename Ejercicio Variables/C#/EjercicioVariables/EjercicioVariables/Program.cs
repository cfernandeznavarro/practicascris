﻿using System;

namespace EjercicioVariables
{
    class Program
    {
        static void Main(string[] args)
        {

            //FASE 1 (3 punts)

            string name = "Cristina";

            string surname1 = "Fernandez";

            string surname2 = "Navarro";


            int day = 8;
            int month = 10;
            int year = 2019;

            Console.WriteLine(surname1 + " " + surname2 + " " + name);

            Console.WriteLine(day + "/" + month + "/" + year);


            Console.WriteLine();
            /* FASE 2(3 punts)
          
            Sabiendo que en 1948 es un año bisiesto:
            ● Cree una variable constante con el valor del año(1948).
            ● Cree una variable constante que guarde cada cuando hay un año bisiesto.
            ● Calcular cuantos años bisiestos hay entre 1948 y su año de nacimiento y almacene el valor
            resultando en una variable.
            ● Muestre por pantalla el resultado del cálculo.*/

            Console.WriteLine("Fase 2");
            const int yearleap = 1948;

            

            const int myBirthday = 1986;

            int contador = 0;

            for (int yearl = 1948; yearl <= myBirthday; yearl++)

            {
              if (DateTime.IsLeapYear(yearl))
                 {

                    Console.WriteLine("{0} is a leap year.", yearl);
                   
                }

                contador++;

            }

            Console.WriteLine();

            /*FASE 3 (3 punts)
             * Partint de l’any 1948 heu de fer un bucle for i mostrar els anys de traspàs fins arriba al vostre any
                de naixement. (0,75 punts)
                ATENCIO! Haureu de canviar el tipus de variable de l’any 1948 per poder modificar-la.
                ● Creeu una variable bool que sera certa si l’any de naixement és de traspàs o falsa si no ho és.(0,75 punts)
                ● En cas de que la variable bool sigui certa, heu de mostrar per consola una frase on ho digui, en cas de ser
                falsa mostrareu la frase pertinent. Creeu dues variables string per guardar les frases. (1,5 punts) */

            Console.WriteLine("Fase 3");









            Console.WriteLine();

            /* FASE 4 (1 punt)
             ● Creeu una variable on juntareu el nom i els cognoms (tot en una variable) i un altre on juntareu la data
             de naixement separada per “/” (tot en una variable). Mostreu per consola les variables del nom complet,
             la data de naixement i si l’any de naixement es de traspàs o no.
             Exemple de sortida per consola:
             El meu nom és Juan Perez Gonzalez
             Vaig néixer el 01/01/1979
             El meu any de naixement és de traspàs.*/
            
            Console.WriteLine("Fase 4");













        }
    }
}
